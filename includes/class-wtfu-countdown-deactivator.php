<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://alexcoleman.io
 * @since      1.0.0
 *
 * @package    Wtfu_Countdown
 * @subpackage Wtfu_Countdown/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Wtfu_Countdown
 * @subpackage Wtfu_Countdown/includes
 * @author     Alex Coleman <alex@alexcoleman.io>
 */
class Wtfu_Countdown_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
