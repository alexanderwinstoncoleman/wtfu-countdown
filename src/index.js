const moment = require("moment");

// CLOCK VARS
const hoursSpan = document.getElementById("ct-hours");
const minutesSpan = document.getElementById("ct-minutes");
const secondsSpan = document.getElementById("ct-seconds");

const interval = 15;
// ///////////////
var firstSlot = moment().set("hour", 6).set("minute", 15).set("second", 0);
var lastSlot = moment().set("hour", 22).set("minute", 15).set("second", 0);
// ///////////////

// SLOTS
const slotList = document.getElementById('slots');
//stackoverflow.com/questions/49079315/15-minute-countdown-timer
function timerFunc() {
	let now = moment();
	let duration;
	let nextSlot;
	if(now.isSame(firstSlot, 'second') || now.isSame(lastSlot, 'second')) {
		displayTimeSlots();
	}
	if(!now.isBetween(firstSlot, lastSlot)){
		nextSlot = firstSlot;
	} else {
		duration = moment.duration(interval, "minutes");
    	nextSlot = moment(Math.ceil(+now / +duration) * +duration);
	}


	// Find the distance between now and the count down date
	var distance = nextSlot.diff(now, "seconds") + 1; // had to add a second to make my brain work.
	var clockTime = moment.utc(distance * 1000);

	hoursSpan.innerHTML = clockTime.format("HH");
	minutesSpan.innerHTML = clockTime.format("mm");
	secondsSpan.innerHTML = clockTime.format("ss");
	if( distance == (60 * interval) ) {
		var id = now.format("h:mma");
		var el = document.getElementById(id);
		if (el) {
			el.classList.add("delete");
			setTimeout(function () {
				el.parentNode.removeChild(el);
			}, 1000);
			clearInterval(myTimer);
			myTimer = setInterval(timerFunc, 1000);
		}
	}
}
let myTimer;
myTimer = setInterval(timerFunc, 1000);

function round( date, duration = moment.duration(interval, "minutes"), method = "ceil" ) {
  	return moment(Math[method](+date / +duration) * +duration);
}

// SLOTS
function intervals() {
	var result = [];
	var current = moment();
	var next15 = round(current);
	while (next15.isBetween(firstSlot, lastSlot)) {
		result.push(next15.format("h:mma"));
		next15.add(interval, "minutes");
	}
	if(!next15.isBetween(firstSlot, lastSlot)) {
		result.push(firstSlot.format("h:mma"));
	}

	return result;
}

function displayTimeSlots(){
	let array = intervals();
	for (let i = 0; i < array.length; i++) {
      listItem(array[i]);
    }

}

function listItem(slot){
	let node = document.createElement("LI");
	var text = document.createTextNode(slot);
	node.appendChild(text);
	node.setAttribute("id", slot);
	slotList.appendChild(node);
}

displayTimeSlots();
