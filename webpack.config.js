// webpack.config.js
var webpack = require("webpack");
var path = require("path");
// const HtmlWebPackPlugin = require( 'html-webpack-plugin' );
const MomentLocalesPlugin = require("moment-locales-webpack-plugin");

module.exports = {
  watch: true,
  devtool: "source-map",
  entry: ["./src/index.js"],
  output: {
    path: path.resolve(__dirname, "public/js"),
    filename: "counter.min.js",
  },
  plugins: [
    // To strip all locales except “en”
    new MomentLocalesPlugin(),
  ],
};
